#!/bin/sh

for host in px0 px1 ma0 ma1 ma2 kn0 kn1 kn2 db0 db1 db2 stor0 stor1 stor2; do
  ssh -o "StrictHostKeyChecking no" $host exit
done
