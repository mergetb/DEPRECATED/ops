#!/usr/bin/env bash

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# build and deploy the mergetb/netc container
#
#

set -e

docker build $BUILD_ARGS -f netc.dock -t quay.io/mergetb/netc:latest .

if [[ ! -z "$PUSH" ]]; then
  docker push quay.io/mergetb/netc:latest
fi

