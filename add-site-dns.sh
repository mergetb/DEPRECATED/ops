#!/bin/bash

if [[ $# -ne 3 ]]; then
  echo "usage: bootstrap-node.sh <name> <domain> <ip>"
  exit 1;
fi

name=$1
domain=$2
ip=$3

raddr=`echo $ip | awk -F. '{print $4"."$3"."$2"."$1}'`


cat <<EOF > /tmp/nsdata
server ns0.portal.$domain
zone $domain

update delete $name.$domain. A
update add $name.$domain. 86400 A $ip

show
send
EOF

nsupdate -k /var/named/ddns-key /tmp/nsdata
