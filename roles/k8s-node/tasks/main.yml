# kubernetes worker node setup
---

# install required software

- name: install required software
  package:
    name: "{{ item }}"
    state: latest
  with_items:
    - socat
    - iptables
    - bind-utils
    - ipset
    - conntrack-tools
  tags:
    - k8n

- name: get kubernetes tarball
  get_url:
    url: https://github.com/kubernetes/kubernetes/releases/download/{{kube_release}}/kubernetes.tar.gz
    dest: /tmp/kubernetes.tar.gz
  tags:
    - k8n

- name: extract kubernetes tarball
  unarchive:
    src: /tmp/kubernetes.tar.gz
    dest: /tmp
    remote_src: yes
  tags:
    - k8n

- name: get kubernetes binaries
  shell: ./kubernetes/cluster/get-kube-binaries.sh
  args:
    chdir: /tmp
    creates: /tmp/kubernetes/server/kubernetes-server-linux-amd64.tar.gz
  environment:
    KUBERNETES_SKIP_CONFIRM: yes
  tags:
    - k8n

- name: extract kubernetes binaries
  unarchive:
    src: /tmp/kubernetes/server/kubernetes-server-linux-amd64.tar.gz
    dest: /tmp/kubernetes/server
    remote_src: yes
  args:
    creates: /tmp/kubernetes/server/kubernetes
  tags:
    - k8n

- name: install kubernetes binaries
  copy:
    src: /tmp/kubernetes/server/kubernetes/server/bin/{{ item }}
    dest: /usr/local/bin/{{ item }}
    remote_src: yes
    mode: 0755
  with_items:
    - apiextensions-apiserver
    - cloud-controller-manager
    - hyperkube
    - kubeadm
    - kube-aggregator
    - kube-apiserver
    - kube-controller-manager
    - kubectl
    - kubelet
    - kube-proxy
    - kube-scheduler
    - mounter
  tags:
    - k8n

- name: get runc binary
  get_url:
    url: https://github.com/opencontainers/runc/releases/download/{{runc_version}}/runc.amd64
    dest: /usr/local/bin/runc
    mode: 0755
  tags:
    - k8n

# setup worker nodes

- name: get k8s node plane binaries
  get_url:
    url: "{{ kube_basepath }}/{{ item }}"
    dest: /usr/bin/{{ item }}
    mode: 0755
  with_items:
    - kubectl
    - kube-proxy
    - kubelet
  tags:
    - k8n

- name: set selinux permissions on kube binaries
  shell: chcon -t bin_t -r object_r -u system_u /usr/bin/kube*
  tags:
    - k8n

- name: get cni and containerd release packages
  get_url:
    url: "{{ item }}"
    dest: /tmp/
  with_items:
    - "{{ bin_urls.cni }}"
    - "{{ bin_urls.containerd }}"
  tags:
    - k8n

- name: create installation directories
  file:
    path: "{{ item }}"
    state: directory
  with_items:
    - /etc/cni/net.d
    - /etc/containerd
    - /opt/cni/bin
    - /var/lib/kubelet
    - /var/lib/kube-proxy
    - /var/lib/kubernetes
    - /var/run/kubernetes
  tags:
    - k8n

- name: extract cni
  unarchive:
    src: /tmp/cni-plugins-amd64-{{ cni_version }}.tgz
    dest: /opt/cni/bin/
    remote_src: yes
  tags:
    - k8n

- file:
    path: /tmp/containerd
    state: directory
  tags:
    - k8n

- name: extract containerd
  unarchive:
    src: /tmp/containerd-{{ containerd_version }}.linux-amd64.tar.gz
    dest: /tmp/containerd
    remote_src: yes
  tags:
    - k8n

- name:
  copy:
    src: /tmp/containerd/bin/{{item}}
    dest: /bin/{{item}}
    mode: 0755
    remote_src: yes
  with_items:
    - containerd
    - containerd-release
    - containerd-shim
    - containerd-stress
    - ctr
  tags:
    - k8n


- name: install containerd config
  copy:
    src: containerd-config.toml
    dest: /etc/containerd/config.toml
  tags:
    - k8n

- name: install containerd systemd file
  template:
    src: containerd.service.j2
    dest: /etc/systemd/system/containerd.service
  tags:
    - k8n

- name: configure cni bridge
  template:
    src: 10-bridge.conf.j2
    dest: /etc/cni/net.d/10-bridge.conf
  vars:
    index: "{{ inventory_hostname_short[-1] }}" #XXX this index will fail if > 9
  tags:
    - k8n

- name: configure cni loopback
  copy:
    src: 99-loopback.conf
    dest: /etc/cni/net.d/99-loopback.conf
  tags:
    - k8n

- name: create kubelet config
  template:
    src: kubelet-config.yaml.j2
    dest: /var/lib/kubelet/kubelet-config.yaml
  vars:
    index: "{{ inventory_hostname_short[-1] }}" #XXX this index will fail if > 9
  tags:
    - k8n

- name: copy kubelet keys
  copy:
    src: /tmp/keygen/{{ item }}
    dest: /var/lib/kubelet/{{ item }}
  with_items:
    - "{{ inventory_hostname_short }}-key.pem"
    - "{{ inventory_hostname_short }}.pem"
  tags:
    - k8n

- name: copy kube-proxy keys
  copy:
    src: /tmp/keygen/{{ item }}
    dest: /var/lib/kube-proxy/{{ item }}
  with_items:
    - kube-proxy-key.pem
    - kube-proxy.pem
  tags:
    - k8n

- name: copy apiserver certificate authority
  copy:
    src: /tmp/keygen/ca.pem
    dest: /var/lib/kubernetes/ca.pem
  tags:
    - k8n

# kubeconfig

- name: delete any existing kubeconfig
  file:
    path: /tmp/{{ inventory_hostname_short }}.kubeconfig
    state: absent
  tags:
    - k8n

- name: generate kubeconfig for cluster
  shell: >
    kubectl config set-cluster mergetb
    --certificate-authority=/var/lib/kubernetes/ca.pem
    --embed-certs=true
    --server=https://px0:6443
    --kubeconfig={{ inventory_hostname_short }}.kubeconfig
  args:
    chdir: /tmp
  tags:
    - k8n

- name: generate kubeconfig for credentials
  shell: >
    kubectl config set-credentials system:node:{{ inventory_hostname_short }}
    --client-certificate=/var/lib/kubelet/{{ inventory_hostname_short }}.pem
    --client-key=/var/lib/kubelet/{{ inventory_hostname_short }}-key.pem
    --embed-certs=true
    --kubeconfig={{ inventory_hostname_short }}.kubeconfig
  args:
    chdir: /tmp
  tags:
    - k8n

- name: generate kubeconfig for context
  shell: >
    kubectl config set-context default
    --cluster=mergetb
    --user=system:node:{{ inventory_hostname_short }}
    --kubeconfig={{ inventory_hostname_short }}.kubeconfig
  args:
    chdir: /tmp
  tags:
    - k8n

- name: set context default in kubeconfig
  shell: >
    kubectl config use-context default --kubeconfig={{ inventory_hostname_short }}.kubeconfig
  args:
    chdir: /tmp
  tags:
    - k8n

- name: install kubeconfig
  copy:
    src: /tmp/{{ inventory_hostname_short }}.kubeconfig
    dest: /var/lib/kubelet/kubeconfig
    remote_src: yes
  tags:
    - k8n

# proxyconfig
 
- name: delete any existing kubeconfig
  file:
    path: /tmp/kube-proxy.kubeconfig
    state: absent
  tags:
    - k8n

- name: generate proxyconfig for cluster
  shell: >
    kubectl config set-cluster mergetb
    --certificate-authority=/var/lib/kubernetes/ca.pem
    --embed-certs=true
    --server=https://px0:6443
    --kubeconfig=kube-proxy.kubeconfig
  args:
    chdir: /tmp
  tags:
    - k8n

- name: generate proxyconfig for credentials
  shell: >
    kubectl config set-credentials kube-proxy
    --client-certificate=/var/lib/kube-proxy/kube-proxy.pem
    --client-key=/var/lib/kube-proxy/kube-proxy-key.pem
    --embed-certs=true
    --kubeconfig=kube-proxy.kubeconfig
  args:
    chdir: /tmp
  tags:
    - k8n

- name: generate proxyconfig for context
  shell: >
    kubectl config set-context default
    --cluster=mergetb
    --user=kube-proxy
    --kubeconfig=kube-proxy.kubeconfig
  args:
    chdir: /tmp
  tags:
    - k8n

- name: set context default in proxyconfig
  shell: >
    kubectl config use-context default --kubeconfig=kube-proxy.kubeconfig
  args:
    chdir: /tmp
  tags:
    - k8n

- name: install proxyconfig
  copy:
    src: /tmp/kube-proxy.kubeconfig
    dest: /var/lib/kube-proxy/kubeconfig
    remote_src: yes
  tags:
    - k8n

# systemd services

- name: create kubelet systemd service
  template:
    src: kubelet.service.j2
    dest: /etc/systemd/system/kubelet.service
  tags:
    - k8n

- name: create kube-proxy systemd service
  template:
    src: kube-proxy.service.j2
    dest: /etc/systemd/system/kube-proxy.service
  tags:
    - k8n

- name: enable services
  systemd:
    name: "{{ item }}"
    enabled: yes
    daemon_reload: yes
  with_items:
    - containerd
    - kubelet
    - kube-proxy
  tags:
    - k8n

- name: start services
  systemd:
    name: "{{ item }}"
    state: restarted
  with_items:
    - containerd
    - kubelet
    - kube-proxy
  tags:
    - k8n


# pod networking
- name: copy routes script
  template:
    src: routes.sh.j2
    dest: /usr/bin/k8s-routes
    mode: '0775'
  vars:
    N: "{{k8s.topo.workers|length}}"
    INDEX: "{{ inventory_hostname_short[2:] | int }}"
  tags:
    - k8n

- name: install route service
  copy:
    src: k8s-routes.service
    dest: /lib/systemd/system/k8s-routes.service

- name: refresh systemd
  systemd:
    name: k8s-routes
    enabled: true
    daemon_reload: true

- name: run routes script
  service:
    name: k8s-routes
    state: restarted

- name: configure cluster dhcp interface
  template:
    src: ifcfg-dhcp.j2
    dest: /etc/sysconfig/network-scripts/ifcfg-{{k8s.worker.ifx}}:0
  vars:
    index: "{{ inventory_hostname_short[-1] | int }}"
  tags:
    - k8n

- name: configure cluster static interface
  template:
    src: ifcfg-static.j2
    dest: /etc/sysconfig/network-scripts/ifcfg-{{k8s.worker.ifx}}:1
  vars:
    index: "{{ inventory_hostname_short[-1] | int }}"
  tags:
    - k8n

- name: bring up dhcp cluster interfaces
  shell: "{{item}} {{k8s.worker.ifx}}:0"
  with_items:
    - ifdown
    - ifup
  tags:
    - k8n

- name: bring up static cluster interfaces
  shell: "{{item}} {{k8s.worker.ifx}}:1"
  with_items:
    - ifdown
    - ifup
  tags:
    - k8n


