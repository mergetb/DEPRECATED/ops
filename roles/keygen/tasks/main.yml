# merge keygen tasks
---

- set_fact:
    api_hosts: "{{ k8s.topo.proxies + k8s.topo.masters + [( k8s.service_cidr | ipaddr('1') | ipaddr('address') )] }}"
    db_hosts: "{{ k8s.topo.proxies + k8s.topo.etcd }}"

- set_fact:
    api_fqdns: "{{ api_hosts | map('regex_replace', '^(.*)$', '\\1.portal.'+ domain) | list }}"
    db_fqdns: "{{ db_hosts | map('regex_replace', '^(.*)$', '\\1.portal.'+ domain) | list }}"

- set_fact:
    api_domains: "{{ api_hosts + api_fqdns }}"
    db_domains: "{{ db_hosts + db_fqdns }}"

- name: create merge directory
  file:
    path: /etc/merge
    state: directory

- name: create merge keys directory
  file:
    path: /etc/merge/keys
    state: directory

- name: copy csr files
  copy:
    src: keygen/
    dest: /etc/merge/keys

# for rvn automation testing
- name: link keygen file to tmp
  file:
    src: /etc/merge/keys
    dest: /tmp/keygen
    state: link

- name: fetch cfssl tool
  get_url:
    url: https://pkg.cfssl.org/R1.2/cfssl_linux-amd64
    dest: /etc/merge/keys/cfssl
    mode: "a+x"

- name: fetch cfssljson tool
  get_url:
    url: https://pkg.cfssl.org/R1.2/cfssljson_linux-amd64
    dest: /etc/merge/keys/cfssljson
    mode: "a+x"

- name: generate certificate authority
  shell: ./cfssl gencert -initca ca-csr.json | ./cfssljson -bare ca
  args:
    chdir: /etc/merge/keys
    creates: /etc/merge/keys/ca.pem

- name: generate k8s api keys
  shell: >
    ./cfssl gencert 
    -ca=ca.pem
    -ca-key=ca-key.pem
    -config=ca-config.json
    -hostname={{ api_domains | join(',') }}
    -profile=mergetb
    kubernetes-csr.json 
    | ./cfssljson -bare kubernetes
  args:
    chdir: /etc/merge/keys
    creates: /etc/merge/keys/kubernetes.pem

- name: generate merge api keys
  shell: >
    ./cfssl gencert 
    -ca=ca.pem
    -ca-key=ca-key.pem
    -config=ca-config.json
    -hostname=api.{{ domain }}
    -profile=mergetb
    merge-api-csr.json 
    | ./cfssljson -bare merge
  args:
    chdir: /etc/merge/keys
    creates: /etc/merge/keys/merge.pem

- name: generate client admin cert
  shell: >
    ./cfssl gencert
    -ca=ca.pem
    -ca-key=ca-key.pem
    -config=ca-config.json
    -profile=mergetb
    admin-csr.json
    | ./cfssljson -bare admin
  args:
    chdir: /etc/merge/keys
    creates: /etc/merge/keys/admin.pem

- name: generate kube-proxy keys
  shell: >
    ./cfssl gencert 
    -ca=ca.pem 
    -ca-key=ca-key.pem 
    -config=ca-config.json 
    -profile=mergetb
    kube-proxy-csr.json | ./cfssljson -bare kube-proxy
  args:
    chdir: /etc/merge/keys
    creates: /etc/merge/keys/kube-proxy.pem

- name: generate worker certificate signing requests
  template:
    src: worker-csr.json.j2
    dest: /etc/merge/keys/{{item}}-csr.json
  with_items: "{{ k8s.topo.workers }}"

- name: generate k8s worker keys
  shell: >
    ./cfssl gencert 
    -ca=ca.pem 
    -ca-key=ca-key.pem 
    -config=ca-config.json 
    -hostname={{item}},{{item}}.portal.{{domain}}
    -profile=mergetb 
    /etc/merge/keys/{{item}}-csr.json | ./cfssljson -bare {{item}}
  args:
    chdir: /etc/merge/keys
    creates: /etc/merge/keys/{{item}}.pem
  with_items: "{{ k8s.topo.workers }}"

- name: generate etcd databse keys
  shell: >
    ./cfssl gencert 
    -ca=ca.pem 
    -ca-key=ca-key.pem 
    -config=ca-config.json 
    -hostname={{ db_domains | join(',') }} 
    -profile=mergetb 
    db-csr.json | ./cfssljson -bare db
  args:
    chdir: /etc/merge/keys
    creates: /etc/merge/keys/db.pem

