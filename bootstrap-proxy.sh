#!/bin/bash

if [[ $# -ne 2 ]]; then
  echo "usage: bootstrap-proxy.sh <config-file> <proxy-index>"
  echo "  config-file: your primary merge configuration file"
  echo "  proxy-index: the index of this proxy (0 or 1)"
  exit 1;
fi

configfile=$1
index=$2

if [[ ! -f ~/.ssh/id_rsa ]]; then
  mkdir -p ~/.ssh
  ssh-keygen -b 2048 -t rsa -f id_rsa -N ''
  mv id_rsa* ~/.ssh/
fi

key=`cat ~/.ssh/id_rsa.pub`
grep -q "$key" ~/.ssh/authorized_keys
key_found=$?
if [[ $key_found -ne 0 ]]; then
  echo "$key" >> ~/.ssh/authorized_keys
fi

cat <<EOF > /etc/ansible/hosts
[portal]
px ansible_host=px$index ansible_user=root ansible_ssh_private_key=/root/.ssh/id_rsa
EOF

# bootstrap my own key
ssh -o "StrictHostKeyChecking no" px$index exit

dnf install -y python2-netaddr python3-netaddr

ansible-playbook \
  --extra-vars=@$configfile \
  --extra-vars "{\"proxy_index\": $index}" \
  px.yml
