# Merge Cluster Documentation

![alt text](./merge-diagram.jpg)
[Diagram Source](https://www.lucidchart.com/documents/edit/2a6d2250-c429-4c98-8c24-ad7ae8579bb4/7)

![alt_text](./merge-11-14-18.jpg)

The merge physical topology is described in our google docs:
[Rack Layout](https://docs.google.com/spreadsheets/d/1Oc5SngtVde0qBHtGxSk-39VA0e-fqDUJjgXMN2b8rx8/edit#gid=0)

Each cable color describes type of data:

 * Blue cables (Data):   10Gb
 * Green cables (Mgmt):  1Gb
 * Orange cables (ipmi): 1Gb
 * Red cables (serial):  1Gb


Switches:

 * hp procurve 2810 24g (data)
 * hp procurve 2810 48g (infra)


Note: The last 4 ports (21-24, 45-48) cannot be used in conjunction with
the gbic ports.
